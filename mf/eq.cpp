#include <iostream>
using namespace std;
#include <stdio.h>
//#include <stdlib.h>
#include "bass.h"
#include <math.h>
 
FILE *output_data;
//char *output_name = "\\home\\home\\Документы\\mf\\1.txt";
//char *input_name = "\\home\\home\\Документы\\mf\\9.mp3";
const char *output_name = "song3.txt";
const char *input_name = "song3.mp3";

//using System;
int main()
{
	output_data = fopen(output_name,"w");
	//output_data = stdout;
 
	BASS_SetConfig(BASS_CONFIG_UPDATEPERIOD,0);         // no audio output, therefore no update period needed
	BASS_Init(0,44100,0,0,NULL);                        // null device, 44100hz, stereo, 16 bits
	DWORD chan = BASS_StreamCreateFile(FALSE,input_name,0,0,BASS_SAMPLE_MONO|BASS_STREAM_DECODE|BASS_STREAM_PRESCAN);     //streaming the file
 
	QWORD pos = BASS_ChannelGetLength(chan,BASS_POS_BYTE);    // вес файла
	cout <<"streaming file ["<<pos<<" bytes]"<<endl;
 
	DWORD p = (DWORD)BASS_ChannelBytes2Seconds(chan, pos);
	printf(" %u:%02u (%i sec)\n", p/60, p%60, p);	// длительность файла в сек
 
	long byte_pos = 0;						// позиция в байтах
	int msec_pos = 0;						// позиция в милисекундах (0.000)
	int msec_interval = 050;				// интервал в милисекундах (0.050)
	long byte_pos_ = 0;						// позиция в интервале
	int msec_pos_ = 0;						// позиция в интервале в милисекундах (0.000)
	//int n = 200;							// шаг Hz  влияет на качество
	int msec_n = 005;						// шаг мс  влияет на качество

	cout<<pos/88200.<<endl;
    //long interval = BASS_ChannelSeconds2Bytes(chan, msec_interval/1000);		// интервал в байтах
	//cout<<"interval "<<interval<<endl;
	#define SIZE 512
	float fft[SIZE];						// спектр
	float peak;								// макс уровень громкости
    float peak2 = 0;						// временный
	float fPeak;							// готовый для плагина (от 0 до 255)
 		
	while (BASS_ChannelIsActive(chan)) {
		if (msec_pos+msec_interval >= pos/88.200)		// если слдующим шагом попадает за конец файла
		//if (msec_pos+msec_interval >= 50000/88.200)
		   break;
		//long byte_pos = BASS_ChannelSeconds2Bytes(chan,fTime);
		msec_pos+=msec_n;
		byte_pos = msec_pos*88.2;		// MSeconds2Bytes
		BASS_ChannelSetPosition(chan, byte_pos, BASS_POS_BYTE);
		//cout<<msec_pos<<" "<<byte_pos<<endl;
		BASS_ChannelGetData(chan, fft, BASS_DATA_FFT1024);	//get the fft data, in this case there are 2048 samples
															//binning the fft, modified from bass spectum.c example in sdk.
					
            	
		peak = 0;
		for (int i=0; i < SIZE-1; i++)
            if (peak < fft[1+i])
                peak = fft[1+i];            // ищем самое большое значение peak на одном спектре

		//if (BASS_ErrorGetCode())
		//	cout<<endl<<byte_pos<<" ERROR "<<BASS_ErrorGetCode()<<endl;
        if (peak2 < peak)					// если в предыдущем спектре было меньше
            peak2=peak;
        //cout<<" msec_pos_ "<<msec_pos_<<endl;
        if (msec_pos_ > msec_interval) {		// если интервал закочился
            msec_pos_ = 0;
			cout<<msec_pos<<" "<<peak2*3<<" "<<byte_pos<<endl;
			fPeak = peak2*1.6*255;
            peak2=0;
			if (fPeak > 255.)
				fPeak = 255;
            fprintf(output_data,"%.0f\n", fPeak);
        } else msec_pos_ += msec_n;
    }
    if(BASS_ErrorGetCode())
        printf("\nERROR %i\n", BASS_ErrorGetCode());
    printf("\nDONE!\n");
    fclose(output_data);
    BASS_Free();
 
    //system("pause");
 
    return 0;
}
