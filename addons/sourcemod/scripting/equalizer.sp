#include	<sdktools>
#include	<sdktools_trace>
#include	<emitsoundany>

#define SIZE 8192
#define STEP 5			// in mSec
//Handle hTimer;

char g_sVolume[SIZE][17];		// 1024 - 102 сек при 10 фпс
int g_iTick = -1000;			// -1000 значит трек не играет, 0 значит начнет играть
int g_iSize;						// размер (количество спектров)
//int ient[MAXPLAYERS+1];
int g_iNeon[MAXPLAYERS+1];		// свет под игрокам
//int g_iEnt;
int g_iStartTime;
int g_iNextTime;
int g_BeamSprite;				// для колец
int g_HaloSprite;				// для колец
char smodel[256] = "materials/sprites/glow04.vmt";
Handle g_hOnPlaying 			= INVALID_HANDLE;
Handle g_hOnStart 				= INVALID_HANDLE;

//#define FILE "aliassounds/musicalrooms/gigidagostino-blablabla.wav"
#define FILE "jb_lego_jail/PatrickReza_feat._Jilian_-_Take_Me_Away.mp3"
//#define FILE "jb_lego_jail/song3.mp3"

public void OnPluginStart()
{
	RegConsoleCmd("sm_qq", CommandSpray);
	g_hOnPlaying = CreateGlobalForward("SL_OnPlaying", ET_Ignore, Param_Float);
	g_hOnStart = CreateGlobalForward("SL_OnStart", ET_Ignore, Param_Float);
	OnMapStart();
	HookEvent("round_prestart", RoundPreStart);
	HookEvent("player_death", Event_PlayerDeath);
}

public OnMapStart()
{
	PrecacheModel(smodel);
	g_BeamSprite = PrecacheModel("materials/sprites/laserbeam.vmt");
	g_HaloSprite = PrecacheModel("materials/sprites/glow01.vmt");
	AddFileToDownloadsTable(FILE);
	PrecacheSoundAny(FILE);
}

public RoundPreStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	g_iTick = -1000;
	for(int i=1;i<MAXPLAYERS;i++)
		RemoveNeon(i);
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	RemoveNeon(GetClientOfUserId(GetEventInt(event, "userid")));
}

//public bool Trace_FilterPlayers(int entity, int contentsMask, any data)
//{
//	return(entity != data && entity > MaxClients) ? true:false;
//}

Handle OpenFile2(const char[] path, const char[] mode)
{
	Handle file = OpenFile(path, mode);
	if (file == INVALID_HANDLE)
		PrintToServer("Failed to open file %s for %s", path, mode);
	else PrintToServer("Opened file handle %x: %s", file, path);
	return file;
}

public Action CommandSpray(client,	args)
{
	g_iTick = -1000;
	char sBuffer[192];
	Format(sBuffer, sizeof(sBuffer), "sound/%s.txt", FILE);
	Handle inf = OpenFile2(sBuffer, "rb");
	//int items[SIZE];
	//char buff[17];
	for (int i=0; i<SIZE; i++) {
		if(IsEndOfFile(inf))
			break;
		ReadFileLine(inf, g_sVolume[i], sizeof(g_sVolume[]));
		TrimString(g_sVolume[i]);
		if(!StringToInt(g_sVolume[i])) {
			g_iSize = i-1;
			break;
		}
		//items[i] = StringToInt(buff);
		//PrintToServer("%i %s", i, g_sVolume[i]);
	}
	//for (int i=0; i<SIZE; i++) {
	//	PrintToServer("%s", g_sVolume[i]);
	//}



	Call_StartForward(g_hOnStart);
	Call_Finish();
	//ient[client] = CreateEntityByName("light_dynamic");
	//if(ient[client] != -1 ) {
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsClientInGame(i) && IsPlayerAlive(i)) {
			RemoveNeon(i);
			float fClientPos[3];
			GetClientAbsOrigin(i, fClientPos);
			int iEnt = CreateEntityByName("light_dynamic");
			DispatchKeyValue(iEnt, "brightness",				"2");
			DispatchKeyValue(iEnt, "spotlight_radius",			"10");
			DispatchKeyValue(iEnt, "distance",					"512");
			DispatchKeyValue(iEnt, "style",						"0");
			DispatchKeyValue(iEnt, "_light",					"255 255 255 255");
			SetEntPropEnt(iEnt, Prop_Send, "m_hOwnerEntity", i);
			if(DispatchSpawn(iEnt)) {
				TeleportEntity(iEnt, fClientPos, NULL_VECTOR, NULL_VECTOR);
				AcceptEntityInput(iEnt, "TurnOn");
				SetVariantString("!activator");
				AcceptEntityInput(iEnt, "SetParent", i, iEnt, 0);
				g_iNeon[i] = iEnt;
			} else g_iNeon[i] = 0;
		}

		//new	Float:vecOrigin[3];
		//char buffer[256]; 
		//Format(buffer, sizeof(buffer), "mus", client); 
		//
		//DispatchKeyValue(client,		"targetname",	buffer); 
//
		//DispatchKeyValue(ient[client], "brightness",				"5");
		//DispatchKeyValue(ient[client], "spotlight_radius",			"1");
		//DispatchKeyValue(ient[client], "distance",					"512");
		//DispatchKeyValue(ient[client], "style",						"0");
		//DispatchKeyValue(ient[client], "_light",					"255 255 255 255");
		//DispatchSpawn(ient[client]);
		//
		//float g_fOrigin[3], g_fAngles[3];
//
		//GetClientEyePosition(client, g_fOrigin);
		//GetClientEyeAngles(client, g_fAngles);
//
		//
		//TR_TraceRayFilter(g_fOrigin, g_fAngles, MASK_SOLID, RayType_Infinite, Trace_FilterPlayers, client);
//
		//if(TR_DidHit(INVALID_HANDLE)) {
		//	TR_GetEndPosition(g_fOrigin, INVALID_HANDLE);
		//	TR_GetPlaneNormal(INVALID_HANDLE, g_fAngles);
		//	GetVectorAngles(g_fAngles, g_fAngles);
		//	g_fAngles[0] += 90.0;
		//}
		//TeleportEntity(ient[client], g_fOrigin, g_fAngles, NULL_VECTOR);
	for(int i=1;i<MAXPLAYERS;i++)
		if(IsClientInGame(i))
			EmitSoundToClientAny(i, FILE);
		//if (hTimer != INVALID_HANDLE){
		//	KillTimer(hTimer);
		//	hTimer = INVALID_HANDLE;
		//g_iEnt = ient[client];
	g_iTick = 0;
	g_iStartTime = GetGameTimeInMS();
	g_iNextTime = g_iStartTime + STEP;
		//PrintToChatAll("%i %i", g_iStartTime, g_iNextTime);

		//}
		//hTimer = CreateTimer(STEP, ChangeEnt, ient[client], TIMER_REPEAT);
	//} else PrintToChat(client, "\x03[NoWallSpray]\x01Ошибка. Не могу создать точку");

	return Plugin_Handled;
}

RemoveNeon(iClient)
{
	if(g_iNeon[iClient] > 0 && IsValidEdict(g_iNeon[iClient])) {
		AcceptEntityInput(g_iNeon[iClient], "Kill");
		RemoveEdict(g_iNeon[iClient]);
	}
	g_iNeon[iClient] = 0;
}

int GetGameTimeInMS()
{
	return RoundToFloor(GetGameTime() * 100);
}

//public Action ChangeEnt(Handle timer, int iEn){
//	if (g_iTick < SIZE){
//		DispatchKeyValue(iEn, "renderamt", g_sVolume[g_iTick]);
//		//PrintToChatAll("%i %s", g_iTick, g_sVolume[g_iTick])
//		g_iTick++;
//	} else if (hTimer != INVALID_HANDLE) {
//		KillTimer(hTimer);
//		hTimer = INVALID_HANDLE;
//		g_iTick = -1000;
//		RemoveEdict(iEn);
//	}
//}

public void OnGameFrame()
{
	if (g_iTick == -1000)
		return;
	if (g_iTick < g_iSize) {
		if (g_iNextTime <= GetGameTimeInMS()) {
			g_iNextTime = GetGameTimeInMS()+ STEP;
			g_iTick = RoundToFloor((GetGameTimeInMS() - g_iStartTime)/float(STEP));
			//PrintToServer("%i", g_iTick);
			if(g_iTick < 0) 
				g_iTick = 0;
			float fVolume = StringToInt(g_sVolume[g_iTick])/255.0;
			Call_StartForward(g_hOnPlaying);
			Call_PushFloat(fVolume);
			Call_Finish();

			char sColor[64];
			FormatEx(sColor, 64, "%s %s %s %s", g_sVolume[g_iTick],  g_sVolume[g_iTick],  g_sVolume[g_iTick],  g_sVolume[g_iTick]);
			//DispatchKeyValue(g_iEnt, "_light", sColor);

			//SetConVarFloat(FindConVar("host_timescale"), StringToFloat(g_sVolume[g_iTick])/170.0);		// будет весело
			//for(int i=1;i<MAXPLAYERS;i++)																	// тоже весело
			//	if(IsClientInGame(i))
			//		SetEntPropFloat(i, Prop_Data, "m_flLaggedMovementValue", StringToInt(g_sVolume[g_iTick])/50.0);
			//if (StringToInt(g_sVolume[g_iTick]) > 100)
			for(int i=1;i<MAXPLAYERS;i++)
				if(IsClientInGame(i) && IsPlayerAlive(i)) {
					//PrintToServer("TE_SetupBeamRing %N", i);
					float fPos[3];
					GetEntPropVector(i, Prop_Send, "m_vecOrigin", fPos);
					fPos[2] += 30.0 *fVolume;
					TE_SetupBeamRingPoint(fPos, 10.0*fVolume, 400.0*fVolume, g_BeamSprite, g_HaloSprite, 0, 10, 0.1, 5.0*fVolume, 10.5*fVolume, {255, 255, 255, 50}, RoundToFloor(5*fVolume), 0);
					TE_SendToAll();
					DispatchKeyValue(g_iNeon[i], "_light",	sColor);
					IntToString(RoundToFloor(1700.0*(fVolume-0.6)), sColor, sizeof(sColor));
					DispatchKeyValue(g_iNeon[i], "distance", sColor);

				}
			//PrintToServer("g_iTick %i %s %f", g_iTick, g_sVolume[g_iTick], GetGameTime() - g_fStartTime);
			//g_iTick++;
		}
	} else {
		PrintToServer("2");
		//RemoveEdict(g_iEnt);
		for(int i=1;i<MAXPLAYERS;i++)
			RemoveNeon(i);
		PrintToServer("g_iTick %i", g_iTick);
		g_iTick = -1000;
	}
}

