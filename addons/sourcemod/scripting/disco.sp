#include <equalizer>
#include <sdktools>

#define PLUGIN_VERSION 		"1.0.273"
//CUSTOMMODEL 		"models/props/slow/spiegelkugel/slow_spiegelkugel.mdl"
//ZOffset 42.0
//sm_disco_rotation 0.0 11.25 0.0
#define MAX_SONGS 		256
char song_name[MAX_SONGS][128];
char song_path[MAX_SONGS][128];
//int song_type[MAX_SONGS];

int g_iDiscoBall	= -1;
int g_iRotating		= -1;
int g_iDust			= -1;
int g_iShake		= -1;
int g_iFog			= -1;
int g_iDiscoColor	= 0;
Handle DiscoTimer = INVALID_HANDLE;
//Cvars
Handle cCustomModel = INVALID_HANDLE;
char sCustomModel[128];
Handle cZOffset = INVALID_HANDLE;
float fZOffset = 0.0;
Handle cRotation = INVALID_HANDLE;
float fRotation[3] = {11.25,...};


new const g_DefaultColors_c[6][3] = { {255,0,0}, {0,255,0}, {0,0,255}, {255,255,0}, {0,255,255}, {255,0,255} };
new const String:g_DefaultColors_p[6][] = { "materials/sprites/redglow1.vmt", "materials/sprites/greenglow1.vmt", "materials/sprites/blueglow1.vmt", "materials/sprites/yellowglow1.vmt", "materials/sprites/glow04.vmt", "materials/sprites/purpleglow1.vmt" };
int g_DefaultColors_s[6];
int g_sprite;

int count;

public Plugin:myinfo =
{
	name = "DISCO!!!",
	author = "MitchDizzle_",
	description = "Mitch's Disco Mod!",
	version = PLUGIN_VERSION,
	url = "https://forums.alliedmods.net/showthread.php?t=180520"
}

public OnPluginStart()
{
	CreateConVar("sm_disco_version", PLUGIN_VERSION, "Disco Mod Version", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	cCustomModel = CreateConVar("sm_disco_custommodel", "models/props/de_train/hr_t/barrel_a/barrel_a.mdl", "Path to the Disco ball model.");
	
	cZOffset = CreateConVar("sm_disco_zoffset", "0.0", "Offset on the Z global vector for the lights.");
	
	cRotation = CreateConVar("sm_disco_rotation", "30.25 30.0 30.25", "Offset on the Z global vector for the lights.");
	
	GetCvars();
	
	//HookConVarChange(g_hcvarCustomModel, ConVarChanged_MDL);
	AutoExecConfig();
	
	
	RegConsoleCmd("sm_discomenu", Command_Disco);
	RegAdminCmd("sm_disco_reloadconfig", Command_ReloadDisco, ADMFLAG_ROOT);
	RegConsoleCmd("say", Command_StopMusic);
	RegConsoleCmd("say_team", Command_StopMusic);
	
	LoadMusicconfig();
	HookEvent("round_end", RoundEnd);
}

GetCvars()
{
	GetConVarString(cCustomModel, sCustomModel, sizeof(sCustomModel));
	PrecacheModel(sCustomModel);
	fZOffset = GetConVarFloat(cZOffset);
	char BuffStr[24];
	GetConVarString(cRotation, BuffStr, sizeof(BuffStr));
	char items[3][8];
	ExplodeString(BuffStr, " ", items, 3, 8);
	fRotation[0] = StringToFloat(items[0]);
	fRotation[1] = StringToFloat(items[1]);
	fRotation[2] = StringToFloat(items[2]);
}

public RoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
	StopAllEntity();

	if(count)
		for(int x = 1; x <= MaxClients; x++)
			if(IsClientInGame(x))
				DoUrl(x, "about:blank");
}

public OnMapStart()
{
	GetCvars();
	g_sprite = PrecacheModel("materials/sprites/laserbeam.vmt", true);
	PrecacheModel(sCustomModel, true);
	for(int i = 0; i <= 5; i++)
		g_DefaultColors_s[i] = PrecacheModel(g_DefaultColors_p[i], true);
		
	StopAllEntity();
}

public OnPluginEnd()
{
	StopAllEntity();
}

public Action:Command_StopMusic(client, args)
{
	// Check to see if client is valid
	if(!(( 1 <= client <= MaxClients ) && IsClientInGame(client))) return Plugin_Continue;

	
	decl String:strMessage[128];
	GetCmdArgString(strMessage, sizeof(strMessage));
	
	// Check for chat triggers
	int startidx = 0;
	if(strMessage[0] == '"') {
		startidx = 1;
		int len = strlen(strMessage);
		
		if(strMessage[len-1] == '"') strMessage[len-1] = '\0';
	}
	
	bool cond = false;
	if(StrEqual("stopmusic", strMessage[startidx], false)) cond = true;
	
	if(!cond)
		return Plugin_Continue;
	else DoUrl(client, "about:blank");
	return Plugin_Continue;
}

stock LoadMusicconfig()
{
	count = 0;
	Handle kvs = CreateKeyValues("MusicConfig");
	decl String:sPaths[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPaths, sizeof(sPaths),"configs/musicconfig.cfg");
	if(!FileToKeyValues(kvs, sPaths)) {
		CloseHandle(kvs);
		return;
	}

	if (!KvGotoFirstSubKey(kvs)) {
		CloseHandle(kvs);
		return;
	}
	count = 1;
	int ocount = -1;
	do {
		if(count != ocount) {
			KvGetSectionName(kvs, song_name[count], 128);
			KvGetString(kvs, "path", song_path[count], 128);
			ocount = count;
			count++;
		}
	} while (KvGotoNextKey(kvs));
	count--;
	CloseHandle(kvs);
	return;
}

public Action:Command_Disco ( client , args )
{
	Void_Menu_Disco(client);
	return Plugin_Handled;
}

public Action:Command_ReloadDisco ( client , args )
{
	LoadMusicconfig();
	GetCvars();
	PrintToChat(client, "\x03[\x04Disco\x03]\x01 The config has been reloaded!");
	
	return Plugin_Handled;
}

stock bool:IsDiscoBall(Ent=-1)
{
	if(Ent != -1)
	{
		//if(IsValidEdict(Ent) && IsValidEntity(Ent) && IsEntNetworkable(Ent))
		//{
		//	decl String:ClassName[255];
		//	GetEdictClassname(Ent, ClassName, 255);
		//	if(StrEqual(ClassName, "disco_ball"))
		//	{
				return (true);
		//}
	}
	return (false);
}

StartStopDisco( client,  Float:fPos[3] , songindex = 0 )
{
	if(IsDiscoBall(g_iDiscoBall)) {
		StopAllEntity();
		DiscoTimer = INVALID_HANDLE;
		PrintToChatAll("\x03[\x04Disco\x03]\x05 %N\x01 has stopped the disco!", client);
		if(count != 0)
			for(int x = 1; x <= MaxClients; x++)
				if(IsClientInGame(x))
					DoUrl(x, "about:blank");
	} else {
		g_iRotating = CreateEntityByName("func_rotating");
		DispatchKeyValue(g_iRotating, "spawnflags", "65");
		DispatchKeyValue(g_iRotating, "maxspeed", "512");
		DispatchSpawn(g_iRotating);
		TeleportEntity(g_iRotating, fPos, NULL_VECTOR, NULL_VECTOR);

		g_iShake = CreateEntityByName("env_shake");
		DispatchKeyValue(g_iShake, "spawnflags", "1");

		DispatchKeyValue(g_iShake, "amplitude", "0");
		DispatchKeyValueFloat(g_iShake, "duration", 0.08);
		DispatchKeyValueFloat(g_iShake, "frequency", 255.0);

		DispatchSpawn(g_iShake );
		TeleportEntity(g_iShake, fPos, NULL_VECTOR, NULL_VECTOR);

		SetVariantString("spawnflags 1");
		AcceptEntityInput(g_iShake, "AddOutput");
		AcceptEntityInput(g_iShake, "StartShake");

		g_iDiscoBall =  CreateEntityByName("prop_dynamic_override");
		DispatchKeyValue(g_iDiscoBall, "classname", "disco_ball");
		DispatchKeyValue(g_iDiscoBall, "model", sCustomModel );
		DispatchKeyValue(g_iDiscoBall, "solid", "0");
		DispatchSpawn(g_iDiscoBall );
		TeleportEntity(g_iDiscoBall, fPos, NULL_VECTOR, NULL_VECTOR );

		ParentEntities(g_iRotating, g_iDiscoBall);

		g_iDust = CreateEntityByName("env_dustpuff");
		DispatchKeyValue(g_iDust, "classname", "disco_fog");
		DispatchSpawn(g_iDust );
		TeleportEntity(g_iDust, fPos, NULL_VECTOR, NULL_VECTOR );

		g_iFog = FindEntityByClassname(MAXPLAYERS, "env_fog_controller");
		if (g_iFog < 1)
			g_iFog = CreateEntityByName("env_fog_controller");
		DispatchKeyValue(g_iFog, "fogenable", "1");
		DispatchKeyValue(g_iFog, "fogblend", "0");
		DispatchSpawn(g_iFog);
		TeleportEntity( g_iFog, fPos, NULL_VECTOR, NULL_VECTOR );
		AcceptEntityInput(g_iFog, "TurnOn");
		PrintToChatAll("%i", g_iFog);

		PrintToChatAll("\x03[\x04Disco\x03]\x05 %N\x01 has started the disco!!", client);
		if(songindex) {
			for(int x = 1; x <= MaxClients; x++)
				if(IsClientInGame(x))
					DoUrl(x, song_path[songindex]);
			PrintToChatAll("\x03[\x04Disco\x03]\x01 Song: \x05%s\x01.", song_name[songindex]);
			PrintToChatAll("\x03[\x04Disco\x03]\x01 To disable the song, type \x05stopmusic\x01 in chat.");
		}

		g_iDiscoColor = 0;
		//DiscoTimer = CreateTimer(0.1, Timer_DiscoUpdate, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
}

public SL_OnPlaying(float fVolume)
{
	if(IsDiscoBall(g_iDiscoBall))
	{
		SetEntityRenderColor(g_iDiscoBall, g_DefaultColors_c[g_iDiscoColor][0], g_DefaultColors_c[g_iDiscoColor][1], g_DefaultColors_c[g_iDiscoColor][2], 255);

		float fPropAngle[3];
		GetEntPropVector(g_iDiscoBall, Prop_Data, "m_angRotation", fPropAngle);
		fPropAngle[0] += fRotation[0] * fVolume;
		fPropAngle[1] += fRotation[1] * fVolume;
		fPropAngle[2] += fRotation[2] * fVolume;
		//TeleportEntity(g_iDiscoBall, NULL_VECTOR, fPropAngle, NULL_VECTOR);
		char sBuffer[64];
		SetVariantFloat(fVolume);
		AcceptEntityInput(g_iRotating, "SetSpeed");

		Format(sBuffer, sizeof(sBuffer), "%.2f", fVolume*100.0);
		DispatchKeyValue(g_iDust, "scale", sBuffer);
		int iVolume = RoundToFloor(fVolume*255);
		Format(sBuffer, sizeof(sBuffer), "%i %i %i %i", iVolume, iVolume, iVolume, iVolume);
		DispatchKeyValue(g_iDust, "color", sBuffer);

		Format(sBuffer, sizeof(sBuffer), "%i", RoundToFloor((fVolume-0.7) * 48.0));
		DispatchKeyValue(g_iShake, "amplitude", sBuffer);
		AcceptEntityInput(g_iShake, "StartShake");

		Format(sBuffer, sizeof(sBuffer), "%i %i %i %i", g_DefaultColors_c[g_iDiscoColor][0], g_DefaultColors_c[g_iDiscoColor][1], g_DefaultColors_c[g_iDiscoColor][2], 255);
		DispatchKeyValue(g_iFog, "fogcolor", sBuffer);
		DispatchKeyValue(g_iFog, "fogcolor2", sBuffer);
		Format(sBuffer, sizeof(sBuffer), "%.2f", (fVolume-0.9)*6.0);
		DispatchKeyValue(g_iFog, "fogmaxdensity", sBuffer);
		Format(sBuffer, sizeof(sBuffer), "%.2f", 1.0);
		DispatchKeyValue(g_iFog, "fogstart", sBuffer);
		Format(sBuffer, sizeof(sBuffer), "%.2f", 1.0);
		DispatchKeyValue(g_iFog, "fogend", sBuffer);

		float fStartPos[3], fEndPos[3];
		GetEntPropVector(g_iRotating, Prop_Send, "m_vecOrigin", fStartPos);
		if(fZOffset != 0)
			fStartPos[2] -= fZOffset;
		TE_SetupGlowSprite(fStartPos, g_DefaultColors_s[g_iDiscoColor], 0.1, 5.0*fVolume, 155 +RoundToFloor(fVolume*100.0));
		TE_SendToAll();
		int iColor[4];
		iColor[3] = 255;
		float fAngles[3];
		Handle hTrace = INVALID_HANDLE;
		float fTracers = fVolume * 8.0;

		while (fTracers > 0) {
			for(int i = 0; i <= 5; i++) {
				fTracers--;
				fAngles[0] = GetRandomFloat(0.0, 90.0);
				fAngles[1] = GetRandomFloat(-180.0, 180.0);
				fAngles[2] = 0.0;
				hTrace = TR_TraceRayFilterEx(fStartPos, fAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
				if(TR_DidHit(hTrace)) {
					TR_GetEndPosition(fEndPos, hTrace);
					iColor[0] = g_DefaultColors_c[i][0];
					iColor[1] = g_DefaultColors_c[i][1];
					iColor[2] = g_DefaultColors_c[i][2];
					LaserP(fStartPos, fEndPos, iColor, fVolume);
					TeleportEntity(g_iDust, fEndPos, NULL_VECTOR, NULL_VECTOR);
					AcceptEntityInput(g_iDust, "SpawnDust");
				}
				CloseHandle(hTrace);
			}	
		}
		g_iDiscoColor++;
		if(g_iDiscoColor > 5) g_iDiscoColor = 0;
		//return Plugin_Continue;
	}
	DiscoTimer = INVALID_HANDLE;
	//return Plugin_Stop;
}

int ParentEntities(int target, int entity)
{
	SetVariantString("!activator"); 
	AcceptEntityInput(entity, "SetParent", target);
}

public bool TraceEntityFilterPlayer(entity, contentsMask)
{
	return (entity > MaxClients || !entity);
}

stock LaserP(const Float:fStartPos[3], const Float:fEndPos[3], const iColor[4], float fVolume)
{
	float fSec = 0.2*fVolume;
	if (fSec<0.1)
		fSec = 0.1;
	TE_SetupBeamPoints(fStartPos, fEndPos, g_sprite, 0, 0, 0, fSec, 6.0*fVolume, 6.0*fVolume, 7, 6.0*fVolume, iColor, 0);
	TE_SendToAll();
}

DoUrl(client, String:url[128])
{
	Handle setup = CreateKeyValues("data");
	
	KvSetString(setup, "title", "DISCO");
	KvSetNum(setup, "type", MOTDPANEL_TYPE_URL);
	KvSetString(setup, "msg", url);
	
	ShowVGUIPanel(client, "info", setup, false);
	CloseHandle(setup);
}


//MENUS
Void_Menu_Disco(client, index=0)
{
	Handle menu = CreateMenu(Menu_Disco, MENU_ACTIONS_DEFAULT);
	SetMenuTitle(menu, "- Disco -");
	if(!IsDiscoBall(g_iDiscoBall))
		AddMenuItem(menu, "StartDisco", "Start Disco", ITEMDRAW_DEFAULT);
	else AddMenuItem(menu, "StopDisco", "Stop Disco", ITEMDRAW_DEFAULT);
	DisplayMenuAtItem(menu, client, index, MENU_TIME_FOREVER);
}

Void_Menu_DiscoSongList(client, index=0)
{
	Handle menu = CreateMenu(Menu_DiscoSL, MENU_ACTIONS_DEFAULT);
	SetMenuTitle(menu, "- Disco Song: -");
	decl String:g_sDisplay[128];
	decl String:g_sChoice[128];
	AddMenuItem(menu, "0", "No Song", ITEMDRAW_DEFAULT);
	for(int X = 1; X <= count; X++) {
		Format(g_sDisplay, sizeof(g_sDisplay), "%s", song_name[X]);
		Format(g_sChoice, sizeof(g_sChoice), "%i", X);
		AddMenuItem(menu, g_sChoice, g_sDisplay, ITEMDRAW_DEFAULT);
	}
	SetMenuExitBackButton(menu, true);
	DisplayMenuAtItem(menu, client, index, MENU_TIME_FOREVER);
}


public Menu_Disco(Handle:menu, MenuAction:action, param1, param2)
{
	switch (action) {
		case MenuAction_End: 	CloseHandle(menu);
		case MenuAction_Select: {
			char info[130];
			GetMenuItem(menu, param2, info, sizeof(info));
			if(StrEqual("StartDisco", info, false)) {
				if(count == 0) {
					float Pos[3];
					float END[3];
					GetEntPropVector(param1, Prop_Send, "m_vecOrigin", Pos);
					float vAngles[3];
					vAngles[0] = -90.0;
					vAngles[1] = 0.0;
					vAngles[2] = 0.0;
					Handle trace = TR_TraceRayFilterEx(Pos, vAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
					if(TR_DidHit(trace))
						TR_GetEndPosition(END, trace);
					CloseHandle(trace);
					if(GetVectorDistance(Pos, END) > 2048.00)
						Pos[2] += 312.0;
					else {
						Pos[0] = END[0];
						Pos[1] = END[1];
						Pos[2] = END[2];
					}
					StartStopDisco(param1, Pos, 0); 
					Void_Menu_Disco(param1);
				} else Void_Menu_DiscoSongList(param1);
			} if(StrEqual("StopDisco", info, false)) {
				StopAllEntity();
				if ( DiscoTimer != INVALID_HANDLE ) {
					KillTimer( DiscoTimer );
					DiscoTimer = INVALID_HANDLE;
				}
				if(count)
					for(int x = 1; x <= MaxClients; x++)
						if(IsClientInGame(x))
							DoUrl(x, "about:blank");
	
				PrintToChatAll("\x03[\x04Disco\x03]\x05 %N\x01 has stopped the disco!", param1);
				Void_Menu_Disco(param1);
			}
		}
	}
	return;
}

void StopAllEntity()
{
	if(g_iRotating) {
		AcceptEntityInput(g_iRotating,	"Stop" );
		AcceptEntityInput(g_iRotating,	"Kill" );
		g_iRotating = -1;
	}
	if(g_iShake) {
		AcceptEntityInput(g_iShake,		"StopShake");
		AcceptEntityInput(g_iShake,		"Kill");
		g_iShake = -1;
	}
	if (g_iDust) {
			AcceptEntityInput(g_iDust, 		"Kill" );
			g_iDust = -1;
	}
	if (g_iDiscoBall) {
		AcceptEntityInput(g_iDiscoBall,	"Kill" );
		g_iDiscoBall = -1;
	}
}

public Menu_DiscoSL(Handle:menu, MenuAction:action, param1, param2)
{
	switch (action) {
		case MenuAction_End: 	CloseHandle(menu);
		case MenuAction_Cancel: Void_Menu_Disco(param1);
		case MenuAction_Select: {
			char info[130];
			GetMenuItem(menu, param2, info, sizeof(info));
			float Pos[3];
			float END[3];
			GetEntPropVector(param1, Prop_Send, "m_vecOrigin", Pos);
			float vAngles[3];
			vAngles[0] = -90.0;
			vAngles[1] = 0.0;
			vAngles[2] = 0.0;
			Handle trace = TR_TraceRayFilterEx(Pos, vAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
			if(TR_DidHit(trace))
				TR_GetEndPosition(END, trace);
			CloseHandle(trace);
			if(GetVectorDistance(Pos, END) > 2048.00)
				Pos[2] += 312.0;
			else {
				Pos[0] = END[0];
				Pos[1] = END[1];
				Pos[2] = END[2];
			}
			StartStopDisco(param1, Pos, StringToInt(info)); 
			Void_Menu_Disco(param1);
		}
	}
	return;
}

